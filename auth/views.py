from django.shortcuts import render, redirect, reverse
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from .forms import UserForm


def index(request):
    response = {}
    if 'failed' in request.GET:
        response['failed'] = True
    elif 'needlogin' in request.GET:
        response['needlogin'] = True
    elif 'loggedout' in request.GET:
        response['loggedout'] = True
    return render(request, 'home.html', response)

def login_view(request):
    user = None
    if 'username' in request.POST and 'password' in request.POST:
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
    if user is not None:
        login(request, user)
        return redirect('/hello')
    else:
        return redirect('/?failed')

def register(request):
    if(request.method == 'POST'):
        form = UserCreationForm(request.POST)
        username = request.POST['username']
        password = request.POST['password']
        if(form.is_valid()):
            User.objects.create_user(username=username, password=password)
            return redirect('/')
    else:
        form = UserCreationForm()
    return render(request, 'signup.html',{'form':form})
        
def hello(request):
    if request.user.is_authenticated:
        response = {
            'name': request.user.name
        }
        if request.user.name == '':
            response['name'] = 'Nameless'
        return render(request, 'home/hello.html', response)
    else:
        return redirect('/?needlogin')

def logout_view(request):
    logout(request)
    return redirect('/?loggedout')